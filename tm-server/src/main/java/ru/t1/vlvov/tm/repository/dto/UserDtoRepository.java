package ru.t1.vlvov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.dto.model.UserDTO;

@Repository
@Scope("prototype")
public interface UserDtoRepository extends AbstractDtoRepository<UserDTO> {

    @Nullable
    UserDTO findFirstById(@NotNull final String id);

    @Nullable
    UserDTO findByLogin(@NotNull String login);

    @Nullable
    UserDTO findByEmail(@NotNull String email);

}
