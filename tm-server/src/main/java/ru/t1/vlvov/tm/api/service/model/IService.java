package ru.t1.vlvov.tm.api.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.vlvov.tm.model.AbstractModel;

import java.util.Collection;
import java.util.List;

public interface IService<M extends AbstractModel> {

    void add(@NotNull final M model);

    void update(@NotNull final M model);

    void set(@NotNull final Collection<M> collection);

    void clear();

    @Nullable
    List<M> findAll();

    @Nullable
    M findOneById(@NotNull final String id);

    void remove(@NotNull final M model);

    void removeById(@NotNull final String id);

    boolean existsById(@NotNull String id);

}
