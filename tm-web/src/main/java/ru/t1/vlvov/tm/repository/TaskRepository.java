package ru.t1.vlvov.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Repository;
import ru.t1.vlvov.tm.module.Task;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
public class TaskRepository {

    private Map<String, Task> tasks = new HashMap<>();

    private final static TaskRepository INSTANCE = new TaskRepository();

    private TaskRepository() {

    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    {
        add(new Task("TEST"));
        add(new Task("BETA"));
        add(new Task("MEGA"));
    }

    public void add(final Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(final Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(@NotNull final String id) {
        return tasks.get(id);
    }

    public void removeById(@NotNull final String id) {
        tasks.remove(id);
    }

    public void create() {
        add(new Task("New Task" + System.currentTimeMillis()));
    }

}
