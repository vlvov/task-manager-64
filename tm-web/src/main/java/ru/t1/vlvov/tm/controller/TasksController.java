package ru.t1.vlvov.tm.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.servlet.ModelAndView;
import ru.t1.vlvov.tm.module.Project;
import ru.t1.vlvov.tm.repository.ProjectRepository;
import ru.t1.vlvov.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.List;

@Controller
public class TasksController {

    @Autowired
    private TaskRepository taskRepository;

    @Autowired
    private ProjectRepository projectRepository;

    @ModelAttribute("projects")
    public Collection<Project> getProjects() {
        return projectRepository.findAll();
    }

    @GetMapping("/tasks")
    public ModelAndView index() {
        return new ModelAndView("task-list", "tasks", taskRepository.findAll());
    }

}
