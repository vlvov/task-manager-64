package ru.t1.vlvov.tm.config;

import org.springframework.context.annotation.ComponentScan;

@ComponentScan("ru.t1.vlvov.tm")
public class ApplicationConfiguration {
}
