package ru.t1.vlvov.tm.listener.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.vlvov.tm.api.endpoint.IDomainEndpoint;
import ru.t1.vlvov.tm.enumerated.Role;
import ru.t1.vlvov.tm.listener.AbstractListener;

@Component
public abstract class AbstractDataListener extends AbstractListener {

    @NotNull
    @Autowired
    protected IDomainEndpoint domainEndpoint;

    @Override
    public @Nullable String getArgument() {
        return null;
    }

    @Override
    public @Nullable Role[] getRoles() {
        return new Role[]{Role.ADMIN};
    }

}
